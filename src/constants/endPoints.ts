const ENDPOINT = import.meta.env.VITE_API_URL;

export const USER_ENDPOINT = ENDPOINT + '/users';
export const TODO_ENDPOINT = ENDPOINT + '/todos';
